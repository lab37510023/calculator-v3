using namespace std;
#include <iostream> 
#include <string> 
#include <vector> 
#include <cassert> 


double add(double res, double num) {
    res += num;
    return res;
}

double subtr(double res, double num) {
    res -= num;
    return res;
}

double mult(double res, double num) {
    res *= num;
    return res;
}

double dev(double res, double num) {
    res /= num;
    return res;
}



void UnitTest() {
    assert(add(24, 123) == 147);
    assert(subtr(1052, 6052) == 5000);
    assert(mult(78, 152) == 11856);
    assert(dev(896, 16) == 56);
    cout << "Successfull passed Unit Tests!" << endl;
}

int main() {
    UnitTest();
    return 0;
}
