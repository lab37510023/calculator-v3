#!/bin/bash
check_copyright() {
        file="$1"
        copyright_pattern="//Copyright(J, K)"
        if grep -q "$copyright_pattern" "$file"; then
                echo "Copyright was found in $file"
        else
                echo "Copyright was not found in $file"
                exit 1
        fi

}

if [$# -ne 1]; then
        echo "Usage: $0 <cpp_file>"
        exit1
fi

cpp_file="$1"
check_copyright "$cpp_file"
