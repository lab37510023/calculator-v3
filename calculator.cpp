//Copyright(J, K)

#include <iostream>
#include <string>
#include <vector>
using namespace std;

double add(double res, double num) {
    res += num;
    return res;
}

double subtr(double res, double num) {
    res -= num;
    return res;
}

double mult(double res, double num) {
    res *= num;
    return res;
}

double dev(double res, double num) {
    res /= num;
    return res;
}

double evaluate(string expr) {
    double result = 0;
    char op = '+';
    int i = 0;
    while (i < expr.length()) {
        if (isdigit(expr[i])) {
            double num = 0;
            while (i < expr.length() && isdigit(expr[i])) {
                num = num * 10 + (expr[i] - '0');
                i++;
            }
            switch (op) {
            case '+': result = add(result, num); break;
            case '-': result = subtr(result, num); break;
            case '*': result = mult(result, num); break;
            case '/': result = dev(result, num); break;
            }
        }
        else if (expr[i] == '+' || expr[i] == '-' || expr[i] == '*' || expr[i] == '/') {
            op = expr[i];
            i++;
        }
        else {
            i++;
        }
    }
    return result;
}

int main() {
    string exp;
    cout << "Enter the expression: ";
    getline(cin, exp);
    //string expr = "2+3*4-16/2";
    double res = evaluate(exp);
    cout << "The result is: " << res;
    return 0;
}


